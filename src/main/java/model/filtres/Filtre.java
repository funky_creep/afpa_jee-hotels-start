package model.filtres;

public class Filtre {

	private int id;
	private String name;
	private boolean isChecked;
	
	
	public Filtre(int id, String nom, boolean isChecked) {
		this.id=id;
		this.name = nom;
		this.isChecked = isChecked;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public boolean isChecked() {
		return isChecked;
	}


	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	
	
	 
	
}
