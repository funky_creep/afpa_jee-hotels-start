package model.filtres;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.model.Collectionnable;


public class ListeFiltres<T extends Collectionnable> {

	private String name;
	private Map<Integer,Filtre> filtres = new HashMap<Integer,Filtre>();
	
	public ListeFiltres(String name, Map<Integer,T> collection) {
		this.name=name;
		for (Entry<Integer, T> e : collection.entrySet()) {
			filtres.put(e.getValue().getId(), new Filtre(e.getValue().getId(), e.getValue().getNom(), false));
		}
	}
	
	public ListeFiltres(String name, Map<Integer,T> collection, List filtresChoisies) {
		this.name=name;
		for (Entry<Integer, T> e : collection.entrySet()) {
			filtres.put(e.getValue().getId(), new Filtre(e.getValue().getId(), e.getValue().getNom(), filtresChoisies.contains(e.getValue().getId()) ? true : false));
		}
	}
	
	public String getName() {
		return name;
	}
	
	public Map<Integer, Filtre> getCollection() {
		return filtres;
	}
	
	public void addItem(Filtre f) {
		filtres.put(f.getId(), f);
	}
	
	public Filtre getItem(Integer itemId) {
		return filtres.get(itemId);
	}
	
	public int getSize() {
		return filtres.size();
	}
	
}
