package servlets;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.ExceptionDBAccess;
import exceptions.ExceptionRequest;
import model.beans.Categorie;
import model.beans.TypeHotel;
import model.dao.DAOCategorie;
import model.dao.DAOTypeHotel;
import model.filtres.ListeFiltres;
import utils.ListeHotels;


/**
 * Servlet implementation class Servlet2
 */

@WebServlet(name = "/indexServlet", urlPatterns = "/*.do")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DAOTypeHotel daotypehotel = new DAOTypeHotel();
		DAOCategorie daocategorie = new DAOCategorie();
		
		// On récupère les filtres choisis par l'utilisateur
		String[] typesHotelChoisis = request.getParameterValues("typehotel");
		String[] categoriesChoisies = request.getParameterValues("categorie");
		
		// On définit comme variables de session
		request.getSession().setAttribute("typesHotelChoisis", typesHotelChoisis);
		request.getSession().setAttribute("categoriesChoisies", categoriesChoisies);
		
		try {
			// On fait passer à la JSP la liste de filtres des types d'hotel
			ListeFiltres<TypeHotel> filtresTypeHotel = typesHotelChoisis == null ? 
														new ListeFiltres<TypeHotel>("typehotel", daotypehotel.findAll().getCollection())
														: new ListeFiltres<TypeHotel>("typehotel", daotypehotel.findAll().getCollection(), Arrays.asList(typesHotelChoisis));
			request.getSession().setAttribute("filtresTypeHotel", filtresTypeHotel);
			
			// On fait passer à la JSP la liste de filtres des catégories de chambre
			ListeFiltres<Categorie> filtresCategorie = categoriesChoisies == null ? 
														new ListeFiltres<Categorie>("categorie", daocategorie.findAll().getCollection())
														: new ListeFiltres<Categorie>("categorie", daocategorie.findAll().getCollection(), Arrays.asList(categoriesChoisies));
			request.getSession().setAttribute("filtresCategorie", filtresCategorie);
			
		} catch (ExceptionRequest e) {
			e.printStackTrace();
		} catch (ExceptionDBAccess e) {
			e.printStackTrace();
		}
		
		ListeHotels fh = new ListeHotels(typesHotelChoisis, categoriesChoisies);
		
		try {
			request.setAttribute("listeHotelsFiltres", fh.trierListeTypeHotel());
		} catch (ExceptionRequest e) {
			e.printStackTrace();
		} catch (ExceptionDBAccess e) {
			e.printStackTrace();
		}

		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
