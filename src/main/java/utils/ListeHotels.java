package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import exceptions.ExceptionDBAccess;
import exceptions.ExceptionRequest;
import model.dao.DAOHotel;

public class ListeHotels {

	private DAOHotel daohotel = new DAOHotel();
	private String[] typesHotelChoisis;
	private String[] categoriesChoisies;
	
	/**
	 * Retourne une liste filtr�e de tous les types d'h�tels non contenus dans le param�tre d'entr�e, cad les id des cases coch�es
	 * @param typesHotelChoisis
	 * @return
	 * @throws ExceptionRequest
	 * @throws ExceptionDBAccess
	 */
	
	public ListeHotels(String[] typesHotelChoisis, String[] categoriesChoisies) {
		this.typesHotelChoisis = typesHotelChoisis != null ? typesHotelChoisis : new String[0];
		this.categoriesChoisies = categoriesChoisies != null ? categoriesChoisies : new String[0];
	}
	
	public List<String> trierListeTypeHotel() throws ExceptionRequest, ExceptionDBAccess {
		return intersectionListes(trierParTypeHotel(typesHotelChoisis), trierParCategorieChambre(categoriesChoisies));
	}
	
	//TODO ajouter une méthode dans DAOHotel pour cette méthode
	private List<String> trierParTypeHotel(String[] typesHotelChoisis) throws ExceptionRequest, ExceptionDBAccess {
		return daohotel.findAll().getCollection().entrySet().stream().filter(x -> Arrays.asList(typesHotelChoisis).contains(Integer.toString(x.getValue().getTypeHotel().getId())))
																	.map(x -> x.getValue().getNomHotel()).collect(Collectors.toList());
	}
	
	private List<String> trierParCategorieChambre(String[] categoriesChambreChoisies) throws ExceptionDBAccess, ExceptionRequest {
		return daohotel.findByRoomCat(categoriesChambreChoisies).getCollection().entrySet().stream()
																	.map(x -> x.getValue().getNomHotel()).collect(Collectors.toList());
	}
	
	/*public List<Object> trierParTypeHotel(String[] typesHotelChoisis) throws ExceptionRequest, ExceptionDBAccess {
		return daohotel.findAll().getCollection().entrySet().stream().filter(x -> Arrays.asList(typesHotelChoisis).contains(Integer.toString(x.getValue().getTypeHotel().getId())))
																	.limit(50)
																	.map(x -> new Object() {
																		String nom = x.getValue().getNomHotel();
																		int typehotel = x.getValue().getTypeHotel().getId();
																	}).collect(Collectors.toList());
	}*/
	
	/**
	 * Prend 2 listes en param�tre et fait l'instersection de ces 2 listes
	 * @param liste1
	 * @param liste2
	 * @return
	 */
	
	//TODO si rien coché renvoyer tout & utiliser stream plutôt que for
	private List<String> intersectionListes(List<String> liste1, List<String> liste2) {
		List<String> listeFinale;
		if (liste1.size() == 0) {
			//System.out.println("liste1 vide");
			listeFinale = liste2;
		}
		else if (liste2.size() == 0) {
			//System.out.println("liste2 vide");
			listeFinale = liste1;
		}
		else {
			//System.out.println("listes pas vides");
			List<String> listeTemp = new ArrayList<String>();
			for (String s : liste1) {
				if (liste2.contains(s) ) {
					listeTemp.add(s);
				}
			}
			listeFinale = listeTemp;
		}
		return listeFinale.stream().limit(50).collect(Collectors.toList());
	}
		
}
