<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="model.beans.TypeHotel, java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Filtrer les h�tels</title>
</head>
<body>
	<% 
		// retour session
		//out.print("requete : " + ((TypeHotel) request.getSession().getAttribute("text")).getNomType());
		// retour request
		//out.print("requete : " + ((TypeHotel) request.getAttribute("text2")).getNomType());
	
		// R�cup�rer plusieurs r�sultats (alors que getValue pour un seul r�sultat) -> Toujours en String !
		//String[] typesHotelChoisis = request.getParameterValues("typehotel");
		
		// R�cup�rer le m�me r�sultat mais dans une variable de session :
		String[] typesHotelChoisis = ((String[]) request.getSession().getAttribute("typehotel"));
		
		//System.out.println(typesHotelChoisis2.length);
		for (String s : typesHotelChoisis) {
			//System.out.println(s);
		}

		//String[] categoriesChoisies = request.getParameterValues("categorie");
		String[] categoriesChoisies = ((String[]) request.getSession().getAttribute("categorie"));
		
		/*for (String s : categoriesChoisies) {
			System.out.println(s);
		}*/
		
		List<String> hotelsFiltres = ((List<String>) request.getAttribute("hotelsFiltres"));
		//System.out.println(hotelsFiltres.size());
		
		//List<String> hotelsFiltres2 = ((List<String>) request.getAttribute("hotelsFiltres2"));
		//System.out.println(hotelsFiltres2.size());
		
	%>
		
	<ul>
	<%
	if (hotelsFiltres.size() != 0) {
		for (String hotel : hotelsFiltres) {%>
		<li><%=hotel%></li>
	<%}
	}
		
	%>
</body>
</html>
