<%@page import="model.utils.*, exceptions.*, model.beans.*, model.dao.*,model.filtres.*,java.util.Map,java.util.List,java.util.stream.Collectors,model.filtres.ListeFiltres,model.filtres.Filtre"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>HomePage</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
		<%
		// On récupère la liste des filtres à afficher 
		ListeFiltres<TypeHotel> filtresTypeHotel = ((ListeFiltres<TypeHotel>) request.getSession().getAttribute("filtresTypeHotel"));
		ListeFiltres<Categorie> filtresCategorie = ((ListeFiltres<Categorie>) request.getSession().getAttribute("filtresCategorie"));
		
		// On récupère la liste des hotels filtrés
		List<String> listeHotels = ((List<String>) request.getAttribute("listeHotelsFiltres"));
		
		// On récupère les précédents choix de l'utilisateur
		String[] typesHotelChoisis = ((String[]) request.getSession().getAttribute("typesHotelChoisis"));
		String[] categoriesChoisies = ((String[]) request.getSession().getAttribute("categoriesChoisies"));
		
		/*if (typesHotelChoisis != null & categoriesChoisies != null) {
			if (typesHotelChoisis.length != 0) {
				System.out.println(typesHotelChoisis.length);
			}
			if (categoriesChoisies.length != 0) {
				System.out.println(categoriesChoisies.length);
			}
		}*/
		%>
		
		<form action="indexServlet" method="POST">
			<h2>Types d'hôtel : </h2>
				<ul>
				<%
					for (Map.Entry<Integer,Filtre> e : filtresTypeHotel.getCollection().entrySet()) {
						//System.out.println(e.getValue().isChecked());
						%>
						<li><label><input type="checkbox" name="<%=filtresTypeHotel.getName()%>" value="<%=e.getValue().getId()%>" <%=e.getValue().isChecked() ? "checked" : ""%>>
						<%=e.getValue().getName()%></label></li>
					<%}
				%>
				</ul>
			
			<h2>Catégories de chambre : </h2>
				<ul>
				<%
					for (Map.Entry<Integer,Filtre> e : filtresCategorie.getCollection().entrySet()) {
						%>
						<li><label><input type="checkbox" name="<%=filtresCategorie.getName()%>" value="<%=e.getValue().getId()%>">
						<%=e.getValue().getName()%></label></li>
					<%}
				%>
				</ul>
			</p>
			<input type="submit" value="Filtrer">
		</form>

	<p>Liste des 50 premiers hôtels filtrés: <br/>
		<ul>
			<%
				for (String item : listeHotels) {
					%>
					<li><%=item%></li>
				<%}
				
			%>
			
		</ul>
	</p>
	
</body>
</html>